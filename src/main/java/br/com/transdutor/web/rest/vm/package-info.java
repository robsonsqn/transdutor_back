/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.transdutor.web.rest.vm;
