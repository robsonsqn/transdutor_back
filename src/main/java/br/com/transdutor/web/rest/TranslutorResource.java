package br.com.transdutor.web.rest;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import br.com.transdutor.domain.Transdutor;
import br.com.transdutor.service.TransdutorService;
import br.com.transdutor.web.rest.util.HeaderUtil;

@RestController
@RequestMapping("/api")
public class TranslutorResource {
	private final TransdutorService transdutorService;

	public TranslutorResource(TransdutorService transdutorService) {
		this.transdutorService = transdutorService;
	}

	@PostMapping("/trandutor")
	@Timed
	public ResponseEntity<Transdutor> makeTransdutor(@RequestBody String entrada) throws URISyntaxException {

		Transdutor transdutor = new Transdutor();
		System.out.println(entrada);

		String[] intermediario = entrada.split(" ");
		transdutor.setEntrada(entrada);
		transdutor.setIntermediario(intermediario);
		transdutor = transdutorService.criarArrayAuxiliar(intermediario, transdutor);

		transdutor = transdutorService.organizaAuxParaVariaveis(transdutor);
		transdutor = transdutorService.organizaSaida(transdutor);
		System.out.println(ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("transdutor", transdutor.getEntrada()))
				.body(transdutor));
		// RETORNAR EM JSON

		return ResponseEntity.created(new URI("/api/addresses/" + transdutor.getSaida()))
		.headers(HeaderUtil.createEntityCreationAlert("Transdutor", transdutor.getSaida().toString())).body(transdutor);
	}
}
